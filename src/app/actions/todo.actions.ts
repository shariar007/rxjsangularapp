import {Action} from "@ngrx/store";
import {TodoActionTypes} from "../shared/enum/todo-action-enum";

export class ActionParent implements Action{
  type: any;
  playload: any;
}

export class TodoAdd implements ActionParent{
  type = TodoActionTypes.add;
  constructor(public playload: any) {}
}

export class TodoRemove implements ActionParent{
  type = TodoActionTypes.remove;
  constructor(public playload: any) {}
}

