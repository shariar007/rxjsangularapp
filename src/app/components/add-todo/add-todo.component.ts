import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {Todo} from "../../models/Todo";
import {TodoAdd} from "../../actions/todo.actions";

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.scss']
})
export class AddTodoComponent implements OnInit {

  constructor(public store: Store<{todo: Todo[]}>) { }

  ngOnInit() {
  }

  addTodo(todoTitle: string){
    const newTodo = new Todo();
    newTodo.title = todoTitle;
    this.store.dispatch(new TodoAdd(newTodo));
  }

  removeTodo(todoTitle: string){
    const newTodo = new Todo();
    newTodo.title = todoTitle;
    this.store.dispatch(new TodoAdd(newTodo));
  }
}
