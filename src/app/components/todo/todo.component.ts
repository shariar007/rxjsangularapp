import { Component, OnInit } from '@angular/core';
import {Todo} from "../../models/Todo";
import {select, Store} from "@ngrx/store";
import { observable } from 'rxjs';
import {TodoRemove} from "../../actions/todo.actions";

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {

  // @ts-ignore
  todoArray: observable<Todo[]>;
  constructor(public store: Store<{todo: Todo[]}>) {
    store.pipe(select('todo')).subscribe(data => {
      this.todoArray = data;
      console.log(this.todoArray);
    });
  }

  ngOnInit() {
  }

  todoRemove(i: number) {
    this.store.dispatch(new TodoRemove((i)));
  }
}
