import {Todo} from "../models/Todo";
import {ActionParent} from "../actions/todo.actions";
import {TodoActionTypes} from "../shared/enum/todo-action-enum";

export const initialState: Todo[] = [
  { title: "Todo 1"},
  { title: "Todo 2"},
  { title: "Todo 3"},
];

export function TodoReducer(state = initialState, action: ActionParent) {
  switch (action.type) {
    case TodoActionTypes.add:
      return [...state, action.playload];
    case TodoActionTypes.remove:
      [...state.splice(action.playload,1)];
      return [...state];
    default: return state;
  }
}
